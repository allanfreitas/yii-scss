yii-scss
========

Scss is an extension for the [Yii PHP framework](http://www.yiiframework.com) that allows developers to use and compile [SCSS](http://sass-lang.com) files into CSS using a server compiler.

### Requirements

* none

### Credits

Thanks to [leaf corcoran](http://leafo.net) for create the [SCSSPHP](http://leafo.net/scssphp/) script that's my base script.

## Usage

### Setup

Download the latest version, unzip the extension under ***protected/extensions/scss*** and add the scss component to your application configuration. 
Below you can find example configurations for both compilers.

## W.I.P.